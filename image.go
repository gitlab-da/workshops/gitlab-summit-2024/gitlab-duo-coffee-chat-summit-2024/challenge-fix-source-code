package main

import (
	_ "image/jpeg"
	_ "image/png"

	"github.com/qeesung/image2ascii/convert"
)

func ConvertImage(path string) string {

	//fmt.Printf("Path: %s", path)

	if path == "" {
		path = "assets/tanuki.png"
	}

	//Image convert options. Use a static size for CI/CD terminal, disable FitScreen (this fails)
	convertOptions := convert.DefaultOptions
	convertOptions.FixedWidth = 100
	convertOptions.FixedHeight = 40
	convertOptions.FitScreen = false
	convertOptions.Colored = true

	converter := convert.NewImageConverter()
	return converter.ImageFile2ASCIIString(path, &convertOptions)
}

func GetAsciiImage(imageStr string, helloFrom string) string {

	introStr := "Hello from " + helloFrom

	asciiImageStr := introStr + "\n\n" + imageStr + "\n\n"

	asciiImageStr += "Everyone can contribute!\n\n"

	asciiImageStr += "Join us at https://about.gitlab.com/community/contribute/"

	return asciiImageStr
}
