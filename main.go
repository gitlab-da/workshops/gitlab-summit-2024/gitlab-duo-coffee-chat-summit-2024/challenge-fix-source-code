package main

/*
import (
	"fmt"
	"os"
)
*/
// [🦊] Tip for Code Suggestions: Type import and wait


var helloFrom = "the GitLab Duo Coffee Chat Challenge at GitLab Summit 2025" // [🦊] Test hint: Look here.

func main() {
	hello_from := os.Getenv("HELLO_FROM")
	image_path := os.Getenv("IMAGE_PATH")

	if hello_from != "" {
		helloFrom = hello_from
	}

	imageStr := ConvertImage(image_path)

	fmt.Println(GetAsciiImage(imageStr, helloFrom))
}
